package com.training.department.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.department.entity.Department;
import com.training.department.service.DepartmentService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("api/departments")
@AllArgsConstructor
public class DepartmentController {

    private static final Logger log = LoggerFactory.getLogger(DepartmentController.class);

    private DepartmentService departmentService;

    @PostMapping
    public ResponseEntity<Department> saveDepartment(@RequestBody Department department){
        log.info("DEPT: Creating department: {}", department);
        Department savedDepartment = null;
        try{
            savedDepartment = departmentService.saveDepartment(department);
            log.info("DEPT: Created successful: {}", savedDepartment);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return new ResponseEntity<>(savedDepartment, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Department> getDepartmentById(@PathVariable("id") Long departmentId){
        log.info("DEPT: Retrieving department with id: {}", departmentId);
        Department department = null;
        try{
            department = departmentService.getDepartmentById(departmentId);
            log.info("DEPT: Retrieved successful: {}", department);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return ResponseEntity.ok(department);
    }
}