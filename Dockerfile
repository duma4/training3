FROM maven:3.8.4-openjdk-17-slim as builder

# Set the working directory inside the container
WORKDIR /app

# Copy the Maven wrapper and project descriptor files
COPY mvnw .
COPY .mvn .mvn

# Copy the project files
COPY pom.xml .
COPY src src

# Build the application
RUN mvn clean package -DskipTests

# Stage 2: Create the Docker image
FROM openjdk:17

# Set the working directory inside the container
WORKDIR /app

# Copy the packaged Spring Boot application JAR file from the previous stage
COPY --from=builder /app/target/department-ms-0.0.1-SNAPSHOT.jar /app/app.jar

# Expose the port on which the Spring Boot application will run
EXPOSE 9999

# Define environment variables (optional)
ENV ENVIRONMENT=""
ENV DATABASE_URL=""
ENV NAMES=""

# Specify any volume for persistence (optional)
VOLUME /app/data

# Run the Spring Boot application when the container starts
CMD ["java", "-jar", "app.jar"]



#docker build --tag dp-ms:latest .
#docker network create mynetwork
#docker run --name dpms --network mynetwork -p 9999:9999 dp-ms
